%-------------- Identification---------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mesTD}[2019/12/13 td ufr maths]
%
\RequirePackage{xkeyval,ifthen}

%-------------- exécution des options -------------------------------
% options par défaut
\ExecuteOptions{10pt,a4paper,pdftex}
%
\newif\ifsol\solfalse
\newif\ifcomment\commentfalse
\newif\ifcours\coursfalse
\newif\ifpazo\pazofalse
\DeclareOption{comment}{\commenttrue}
\DeclareOption{cours}{\courstrue}
\DeclareOption{solution}{\soltrue}
\DeclareOption{nocomment}{\commentfalse}
\DeclareOption{nocours}{\coursfalse}
\DeclareOption{nosolution}{\solfalse}
\DeclareOption{answer}{\soltrue\courstrue\commenttrue}
\DeclareOption{noanswer}{\solfalse\coursfalse\commentfalse}
\DeclareOption{pazo}{\pazotrue} % mathpazo font
%-------------- les autres options sont envoyées à article
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%\DeclareOption*{\ClassWarning{mesTD}{Unknown option '\CurrentOption'}}
\ProcessOptions
\relax
%-------------- chargement d'extensions -----------------------------
\LoadClass{article}

\RequirePackage{geometry}
\RequirePackage{graphicx}

\RequirePackage{fixltx2e} %des bugfixes pour LaTeX
%\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{amssymb}
\RequirePackage[x11names,svgnames,dvipsnames]{xcolor} %pour les couleurs
\ifpazo \RequirePackage{mathpazo} \fi
\RequirePackage[frenchb]{babel}
\RequirePackage{hyperref}
\RequirePackage{booktabs} % professional looking tabulars
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\RequirePackage{multicol}
%\RequirePackage{paralist}
%\RequirePackage{tabto}
\RequirePackage{setspace}

%-------------- macros maths        --------------------------------

\newcommand\N{\mathbb{N}}
\newcommand\Z{\mathbb{Z}}
\newcommand\Q{\mathbb{Q}}
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}

%--------------- mise en page   -------------------------------------

\geometry{height=24cm,width=16cm}

\setlength{\parindent}{0pt}

\def\@title{TD}
\def\@date{Année 2019-2020}
\def\@module{MM1 - Algèbre et analyse élémentaire 1}
\def\@institution{Université de Paris}
%\def\@author{Pascal Molin}

\def\title#1{\gdef\@title{#1}}
\def\date#1{\gdef\@date{#1}}
\def\module#1{\gdef\@module{#1}}
\def\institution#1{\gdef\@institution{#1}}
%\def\author#1{\gdef\@author{#1}}

\newcommand{\makeheader}{
 {
  \bf
  \noindent
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lr}
   \@institution & \@date
   \\
  \@module{} 
  \end{tabular*}
 }
 \begin{center}
  { \Large \bf \@title }
 \end{center}
 $\,$
}

%%%--------------- Environnements ------------------------------------
%%
%% \begin{cours}
%% \end{cours}
%% \begin{indication}
%%
%% \end{indication}
%%
%% \begin{exercice}
%% \qq question
%% \sol{}
%% \end{exercice}
%%
%% \begin{probleme}
%% \partie
%%   \qq  
%% \partie
%% \end{probleme}
%%
%%

\newcounter{qq}
\newcommand{\qq}{\medskip\par{\footnotesize\bfseries \stepcounter{qq}\arabic{qq}. }} 
\newcommand{\qa}{\medskip\par{\footnotesize\bfseries \stepcounter{qq}\alph{qq}. }} 
\newcounter{exo}
\newcommand{\exo}[1][]{\setcounter{qq}{0}\bigskip\par\hspace*{-1ex}{\small \bfseries Exercice \stepcounter{exo}\arabic{exo} : #1 \par\smallskip}}
\newcounter{pbpart}
\newcommand{\probleme}[1][]{\setcounter{qq}{0}\bigskip\par\centerline{\small \bfseries \scshape Problème #1} \par\smallskip}
\newcommand{\partie}[1][]{\bigskip\par\centerline{\small \bfseries Partie \stepcounter{pbpart}\arabic{pbpart} #1} \par\smallskip}

\newenvironment{exercice}{\exo}{}

\RequirePackage[]{todonotes}

\newcommand{\comment}[1]{%
  \ifcomment\todo[inline, backgroundcolor=white, bordercolor=black!25!green,caption={}]{#1}\fi}
\newcommand{\cours}[1]{%
  \ifcours\todo[inline, backgroundcolor=white, bordercolor=black!25!red,caption={}]{{\bfseries Cours: }#1}\fi}
\newcommand{\sol}[1]{%
  \ifsol\todo[inline, backgroundcolor=white, bordercolor=blue,caption={}]{#1}\fi}
\newcommand{\ssol}[1]{\ifsol~{\color{blue}\fbox{#1}}\fi}

%\RequirePackage{framed}
%\newcommand{\sol}[1]{
%\ifsol
%\par\begin{framed} #1 \end{framed}
%%\medskip\par {\em Solution : #1 }
%\else
%\fi
%}

\newcounter{bareme}
\newcounter{pts}
\newcommand\bareme[1]{%
\ifsol
  \marginpar{\footnotesize [
  \ifnum\value{pts} > 0
    \arabic{pts}pts /
  \fi
  sur #1]}
  \addtocounter{bareme}{#1}
  \setcounter{pts}{0}
\fi}
%\newcommand\pts[1]{\ifsol\addtocounter{pts}{#1}\llap{[#1]\hspace{3em}}\fi}
\newcommand\pts[1]{\ifsol\addtocounter{pts}{#1}\marginpar{\footnotesize [ /#1]}\fi}

\newcommand{\indic}[1]{\smallskip\par{\qquad\small\em indication : #1}}
\newcommand{\source}[1]{}


% changement de l'affichage des sections
  \renewcommand{\section}{%
    \@startsection{section}{1}{20pt}{3ex \@plus 1ex \@minus 1ex}{1.5ex}%
    {\normalfont\bfseries\scshape}%
    }
  % et je veux la numérotation dans la marge
  \renewcommand{\@hangfrom}[1]{\setbox\@tempboxa\hbox{{#1}}%
    \hangindent\wd\@tempboxa\noindent\llap{{#1}}}
    %% \@hangfrom est la macro qui affiche les numérotations
    %% on fait une boîte temporaire (commande tex), on récupère sa largeur avec
    % la commande \wd, et on affiche à gauche du point courant avec \llap
\renewcommand{\subsection}{%
    \@startsection{subsection}{1}{20pt}{2ex \@plus 0.75ex \@minus 0.75ex}{1.25ex}%
    {\normalfont\small\bfseries\scshape}%
    }
  % et je veux la numérotation dans la marge
  \renewcommand{\@hangfrom}[1]{\setbox\@tempboxa\hbox{{#1}}%
    \hangindent\wd\@tempboxa\noindent\llap{{#1}}}
    %% \@hangfrom est la macro qui affiche les numérotations
    %% on fait une boîte temporaire (commande tex), on récupère sa largeur avec
   
% les numéros de sections sont affichés sans le numéro de chapitre
% sinon, mettre un \thechapter avant \arabic
\renewcommand{\thesection}{\arabic{section}}

\AtBeginDocument{\makeheader}

\endinput
