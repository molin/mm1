\documentclass[noanswer]{td}
\title{Feuille de TD 1 : Fonctions}
\begin{document}

\comment{
  En guise de syllabus, le cours peut commencer par une étude de
  fonction complète.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fonctions exponentielle, logarithme, puissance.}

\begin{exercice}
Écrire sous forme de fraction et sans exposant les nombres suivants :
\qq   $a=16^{\frac{1}{2}}$
    \ssol{$=4$}
\quad $b=81^{3/4}$
    \ssol{$=27$}
\quad $c=27^{4/3}$
\quad $d=64^{1/3}$
\quad $e=\left(\frac{25}{4}\right)^{3/2}$
    \ssol{$=\left(\frac{5}{2}\right)^{3}=\frac{125}8$}
\qq   $a=(2^{-1/3})^{-21}$
    \ssol{$=2^7=128$}
\quad $b=(0,064)^{-5/3}$
    \ssol{$=(\frac{64}{1000})^{-5/3} = (\frac4{10})^{-5} ={\frac{3125}{32}}$}
\quad $c=5^{-4}\cdot5^2$
\quad $d=(5^{-1}+5^0)^{-1}$
    \ssol{$=5/6$}
\quad $e=7^{-1/2}\cdot 7^{5/2}$
    \ssol{$=7^{4/2} = 7^2 = 49$}
\end{exercice}

\begin{exercice}
Vrai ou faux ?
\qq $\ln(72)=2\ln(3)-3\ln(2)$.
\qq $\ln(0{,}08)=3\ln(2)-2\ln(5)$.
\qq $\ln(0{,}04)=2\ln(2)-2\ln(5)-\ln(4)$.
\end{exercice}

\begin{exercice}
Calculer en fonction de $\ln(2)$ et $\ln(3)$ les expressions suivantes.
$$
\ln(1,5),\hspace{.5cm} \ln(16),\hspace{.5cm} \ln({^3}\sqrt{9}),\hspace{.5cm} \ln(2\sqrt{2}),\hspace{.5cm}\ln(0,25\times e),\hspace{.5cm}\ln\left(\frac{9}{8}\right),\hspace{.5cm}\ln(36 e^2),\hspace{.5cm}\ln\left(\frac{12}{e^{\frac{4}{5}}}\right).
$$
\end{exercice}

\comment{Les puissances d'exposant entier positif, négatif, rationnel sont déjà connues.
  On étend la définition aux exposants irrationnels.}
\cours{
  Soit $\alpha$ un réel. On définit $x^\alpha=e^{\alpha\ln(x)}$ pour tout réel $x$ strictement positif. Étudier les variations de la fonction $]0,+\infty[\rightarrow\mathbb{R}$, $x\mapsto x^\alpha$ en fonction des valeurs de $\alpha$. On tracera les courbes représentatives sur un même graphe dans les différents cas.
}

\begin{exercice}
Soient $a$ et $b$ des réels strictement positifs. Montrer que
pour tout réel $x$, $\displaystyle a^xb^x=(ab)^x$.
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Ensembles de définition}
\comment{ Par opposition aux applications définies en RM1,
  on considère ici qu'une «fonction» est une procédure de calcul.
  Elle est définie là où l'écriture se calcule, avant toute simplification.}

\begin{exercice}
Donner l'ensemble de définition des expressions suivantes
\begin{multicols}{3}
\qq $\displaystyle \frac{x^7-12x+13}{x^2+1}$
\qq $\displaystyle e^{-\frac{1}{x^2}}$
\qq $\displaystyle \frac{\log(x)}{x^2-1}$
\qq $\displaystyle \ln(1-x)$
\qq $\displaystyle 3^{x+7}$
\end{multicols}
\end{exercice}

\begin{exercice}
Donner l'ensemble de définition des expressions suivantes avant et après simplification.\vspace*{-1mm}
\qq $e^{4\ln(x)-\ln x}$.
\qq $e^{\ln(x^2+1)-\ln(x-1)-\ln(x+1)}$.
\qq $e^{\frac{1}{2}\ln(x^2-1)-\ln(x)-\ln(x^2+1)}$.
\end{exercice}

\begin{exercice}
Déterminer l'ensemble où les équations suivantes ont un sens et les résoudre.
\begin{multicols}{2}
\qq $\ln(1-2x)=\ln(x+2)+\ln(3)$.
\qq $\ln(1-x^2)=\ln(2x-1)$.
\qq $\ln (\sqrt{2x-2})=\ln(4-x)-\frac{1}{2}\ln \,x$.
\qq $2e^{2x}-5e^x=-2$.
\qq $e^x-2e^{-x}-1=0$.
\qq $\ln(2-x)\leq \ln (2x+1)-\ln(3)$.
\qq $\ln(3x+2)\geq\ln\left(x^2+\frac{1}{4}\right)$.
\end{multicols}
\end{exercice}

\begin{exercice}
Donner l'ensemble de définition des fonctions suivantes :
\begin{multicols}{3}
\qa $\displaystyle \sqrt{x^2-3x+2}$
\qa $\displaystyle \left(\frac{x^2-1}{x^2-x-2}\right)^{3/2}$
\qa $\displaystyle (x^3-x^2+3x-3)^{-\pi}$
\qa $\displaystyle (x^2+x+1)^{-1/2}$
\qa $\displaystyle 2^x\ln(1+x^2)$
\end{multicols}
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fonctions trigonométriques}
\comment{Pour les deux premières sous-sections, on présente les fonctions
trigonométriques comme des fonctions sur le cercle.}

\subsection{Rappels: cercle trigonométrique}

\begin{exercice}
À partir du cercle trigonométrique, retrouver les relations entre :
\qq $\cos(\pi/2-\theta)$ et $\sin(\theta)$,
\qq $\sin(\pi/2-\theta)$ et $\cos(\theta)$,
\qq $\cos(\pi/2+\theta)$ et $\sin(\theta)$,
\qq $\sin(\pi/2+\theta)$ et $\cos(\theta)$,
\qq $\cos^2(x)$ et $\sin^2(x)$ pour tout $x\in\R$.
\end{exercice}

\begin{exercice}
En utilisant la géométrie du triangle, compléter le tableau suivant, les angles étant mis entre $0$ et $\pi$, dans l'ordre croissant (on précisera si un nombre n'est pas défini) :\vspace*{-2mm}
$$\begin{array}{|c|c|c|c|c|c|c|c|}\hline
 &&&&& &&\\[-3mm] \theta \hbox{ (radians)}&&\displaystyle{\pi\over 6}&\displaystyle{\pi\over 4}&&&&\pi\\[-3mm]
 &&&&& &&\\
\hline
 &&&&& &&\\[-3mm]
\theta \hbox{ (degrés)}&0^\circ&&&60^\circ&&120^\circ&\\[-3mm]
 &&&&& &&\\ \hline
\cos(\theta)&&&&&0&&\\ \hline
\sin(\theta)&&&&&&&\\ \hline
\tan(\theta)&&&&&&&\\ \hline
\end{array}$$
\end{exercice}
\vskip 5pt

\subsection{Equations trigonométriques}

\cours{équations
  $\cos(x)=\cos(y)$,   $\sin(x)=\sin(y)$, $\tan(x)=\tan(y)$
}

\begin{exercice}
Résoudre les équations  suivantes :
\begin{multicols}{3}
\qq $\displaystyle x+2\pi\equiv 2x-{\pi\over 3}\bmod 2\pi$
\qq $\displaystyle 5x \equiv {\pi\over 2}\bmod 2\pi$
\qq $\displaystyle x \equiv {\pi\over 3}-3x\bmod \pi$
\qq $\displaystyle 3x-\pi\equiv 2x-{\pi\over 2}\bmod\pi/2$
\qq $\displaystyle 5x-1 \equiv {\pi\over 2}\bmod\pi/3$
\qq $\displaystyle x\equiv{\pi\over 3}-3x\bmod\pi/4$
\end{multicols}
\end{exercice}

\begin{exercice}
Résoudre les équations  suivantes :
\begin{multicols}{2}
\qq $\displaystyle \cos(2x)=\cos(3x)$
\qq $\displaystyle \sin(x)=\sin\left(2x+{\pi\over 2}\right)$
\qq $\displaystyle \cos(x)= \sin(x)$
\qq $\displaystyle \tan(x)= \tan(3x)$
\end{multicols}
\end{exercice}

\begin{exercice}
Résoudre les équations suivantes :
\begin{multicols}{2}
\qq $\displaystyle 4\cos(x)+1=3$
\qq $\displaystyle 4\cos^2(2x)-1=1$
\qq $\displaystyle (\tan(x)+1)^2-2\tan(x)=4$
\qq $\displaystyle \sin\left(\frac{\pi}{5}-x\right)=\frac{1}{2}$
\end{multicols}
\end{exercice}

\subsection{Fonctions trigonométriques}

\begin{exercice}
Donner l'ensemble de définition des fonctions $x\mapsto\cos(x)$, $x\mapsto\sin(x)$, et  $x\mapsto \tan(x)$ puis donner l'allure de leur courbe représentative à partir des définitions. Quelle opération géométrique permet de passer de la courbe de $\sin$ à celle de $\cos$ ?
\end{exercice}

\begin{exercice}
Donner l'ensemble de définition des expressions suivantes :\vspace*{-3mm}
$$
\sqrt{\ln(\cos (x))},\hspace{.5cm} \frac{1}{\sqrt{\sin(x)}}, \hspace{.5cm} e^{\ln(\tan(x))},\hspace{.5cm}\ln(|\cos(x)|),\hspace{.5cm} \frac{1}{1-\cos(x)}, \hspace{.5cm}\frac{1}{\cos(x)+\sin(x)}
$$
\end{exercice}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Images, antécédents, composées}
\comment{ Si possible, introduire les applications en RM1 avant cette partie.}
 
\begin{exercice} Déterminer, s'ils existent, les antécédents de $1$ par $f:\R\rightarrow\R$ dans les cas suivants :
\begin{multicols}{2}
\qq $f(x)=x^2-3x+4$.
\qq $f(x)=e^x-e^{-x}+4$.
\qq $f(x)=\log_{10}(x^2+1)$.
\qq $f(x)=\tan^2(x)$.
\qq $f(x)=\frac{1}{2}e^{\sqrt{|x^2-1|}}$.
\end{multicols}
\end{exercice}

\begin{exercice} Déterminer l'image de $f:\R\rightarrow\R$ dans les cas suivants :
\qq $f(x)=\cos(x)$.
\qq $f(x)=\sqrt{x^2+1}$.
\qq $f(x)=4x^2+3x+2$ (on pourra mettre l'expression sous sa forme canonique).
\qq $f(x)=-5x^2-5x$.
\qq $f(x)=x(x^2+1)$ (réfléchir plus généralement sur le cas des polynômes de degré $3$).
\qq $f(x)=e^{(x^2-1)}$.
\end{exercice}

\begin{exercice}Calculer  $f\circ g$ et $g\circ f$ dans les cas suivants et vérifier que $f\circ g\neq g\circ f$.
\begin{multicols}{2}
\qq $f(x)=x^2+1$, $g(x)=x^3-3x^2+2$.
\qq $f(x)=e^x$, $g(x)=x^2-3x+4$.
\qq $f(x)=\cos(x)$, $g(x)=e^x$.
\qq $f(x)=|x|$, $g(x)=x+1$.
\end{multicols}
\end{exercice}

\begin{exercice}Soient $f,g:\R\rightarrow\R$. Donner l'ensemble de définition de $g\circ f$ dans les cas suivants.
\qq $f(x)=x^3-1$, $g(x)=\sqrt{x}$.
\qq $f(x)=\cos (x)$, $g(x)=\ln(x)$.
\qq $f(x)=x^2-1$, $g(x)=\frac{\sqrt{x}}{x}$.
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Applications réciproques}
\comment{La définition relève plutôt de RM1}

%\cours{
%Soient $A$ et $B$ deux intervalles de $\R$.
%\begin{enumerate}
%\qq Montrer qu'une fonction $f:A\rightarrow B$ est bijective si et seulement si il existe une fonction $g:B\rightarrow A$ telle que
%\vskip-4mm
%\begin{equation}
%\label{reciproque}
%g\circ f={\rm Id}_A,\hspace{1cm} f\circ g={\rm Id}_B,
%\end{equation}
%avec ${\rm Id}_A:A\rightarrow A$, $x\mapsto x$.
%\qq Si elle existe, la fonction $g$ ci-dessus est-elle aussi bijective ?
%Deux fonctions $f$ et $g$ qui vérifient (\ref{reciproque}) sont dites \emph{réciproques}, $g$ s'appelle l'inverse de $f$ et est notée $f^{-1}$ (de même $f$ est l'inverse de $g$ et est notée $g^{-1}$).
%\end{enumerate}
%}

\begin{exercice}
Donner deux intervalles $A$ et $B$ et deux fonctions $f:A\rightarrow B$ et $g:B\rightarrow A$ réciproques (l'une de l'autre) dans les cas suivants.
\begin{multicols}{3}
\qq $f:x\mapsto x^2$.
\qq $f:x\mapsto x^3+1$.
\qq $f:x\mapsto \ln(x)$.
\qq $f:x\mapsto e^{2x}$.
\qq $f:x\mapsto x^2+3x-4$.
\qq $f:x\mapsto x^4+x^2+1$.
\end{multicols}
\end{exercice}

\begin{exercice}
\qq Montrer que si $f$ et $g$ sont deux fonctions réciproques l'une de l'autre alors le graphe de $f$ est le symétrique du graphe de $g$ par la droite $y=x$.
\qq Donner des exemples de fonctions $f:A\rightarrow A$ telles que $f^{-1}=f$.
\end{exercice}

\begin{exercice}
    Soit $a>0$, on appelle {\em exponentielle en base $a$} l'application
    $\exp_a:\R\to\R$ définie par $x\mapsto a^x$.
\qq Supposons $a\neq 1$. Montrer que la fonction $\log_a:]0,+\infty[\rightarrow\mathbb{R}$, $x\mapsto \frac{\ln(x)}{\ln(a)}$ vérifie\vspace*{-1mm}
$$
\log_a(\exp_a(x))=x,\hspace{2cm} \text{ pour tout }x\in \mathbb{R}.\vspace*{-3mm}
$$
et\vspace*{-5mm}
$$
\exp_a(\log_a(x))=x,\hspace{2cm}\text{ pour tout }x\in]0,+\infty[.
$$
En déduire les variations de la fonction $\log_a$ en fonction des valeurs de $a$.

La fonction $\log_a$ s'appelle {\em logarithme en base $a$}.

\qq Sur un même graphe, tracer approximativement en fonction des valeurs de $a$ les courbes représentatives des fonctions $\exp_a$ et $\log_a$.
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limites, asymptotes}

\begin{exercice}
Représentez graphiquement, dans chaque cas ci-dessous, une fonction qui satisfait les conditions suivantes.
\qa $\displaystyle\lim_{0^-}f(x)=-1$, $\displaystyle\lim_{0^+}f(x)=2$, $f(0)=1.$
\qa $\displaystyle\lim_{0^-}f(x)=2^+$, $\displaystyle\lim_{0^+}f(x)=0^-$, $\displaystyle\lim_{4^-}f(x)=3$, $\displaystyle\lim_{4^+}f(x)=+\infty$, $f(5)=1$, $f(0)=2.$
\end{exercice}

\begin{exercice}
Calculer les limites suivantes, si elles existent :
    \comment{Limites simples, pas de forme indéterminée.
    On admet que $\sin(x)$ n'a pas limite en l'infini.}
\begin{multicols}{3}
\qq $\displaystyle \lim_{x\to 0} x^2-2x$, %Condamine p.139
\qq $\displaystyle \lim_{x\to 1} x^2-4x+3$, %Condamine p.139
\qq $\displaystyle \lim_{x\to 2} x^2-2x$, %Condamine p.139
\qq $\displaystyle \lim_{x\to 1} \frac{\sqrt{x+2}-3}{x-3}$,
%limite en $\pm _infty$
\qq $\displaystyle \lim_{x\to +\infty} \sqrt{x^2-1}$, %Condamine p.139
%\qq $\displaystyle \lim_{x\to -\infty} \sqrt{2x^2+3}$,
\qq $\displaystyle \lim_{x\to -\infty}\sqrt{x^2-3}$,
\qq $\displaystyle \lim_{x\to +\infty}1+x+\ln x$,
\qq $\displaystyle \lim_{x\to+\infty} x^4e^x\ln^2 x$, % %V. Collet ex.10,11 p.67
\qq $\displaystyle \lim_{x\to +\infty} xe^{\frac1x}$,
%limites en $x_0^+$ et $x_0^-$
\qq $\displaystyle \lim_{x\to 1^-}\frac1{x^4-1}$, %analyse BCDE 2.11 p. 27
\qq $\displaystyle \lim_{x\to 1^+}\dfrac1{\sqrt{x^2-1}}$,
\qq $\displaystyle \lim_{x\to 1^-}\dfrac1{(x^2-1)(x+2)},$ %moi
\qq $\displaystyle \lim_{x\to 1} \cos\left(e^{-\frac{1}{(x-1)^2}}\right)$,
\qq $\displaystyle \lim_{x\to +\infty} \dfrac{1+\tan\frac1x}{\cos\left(\frac{\pi}2-\frac1{x}\right)}$, %moi $=-\infty$
\qq $\displaystyle \lim_{x\to 0^+} \dfrac1{x^2}e^{\frac1x}$, %Condamine p.492
\qq $\displaystyle \lim_{x\to 0^+} \dfrac{e^{\frac1{x^2}}+\frac1{x^2}+x}{\sin x}$,
%\qq $\displaystyle \lim_{x\to 0} \dfrac{1}{1+e^{\frac1x}}$, %Condamine p.492
%\indic{calculer les limites de $f$ en $0^+$ et $0^-$.}
\qq $\displaystyle \lim_{x\to 0^+} \sin \dfrac1x$,
%\indic{remarquer que $\sin x=\sin\left(\dfrac1x\right)\circ \left(\dfrac1x\right).$}
\qq $\displaystyle \lim_{x\to 0} \dfrac{x}{1+e^{\frac1x}}$, %Condamine p.492
%formes non indéterminées (0^+)^{+\infty}->0 et (0^+)^{-\infty}->+\infty$
%à Rajouter
\qq $\displaystyle \lim_{x\to 1^+} (x-1)^{\frac2{x-1}}$,
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{2x^2+1}{x^2+2}$,
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x}{x^2+1}$,
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^2+x}{x-1}$,
%\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^2-x}{x+2}$,
%\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^3-2}{x^3+1}$.
\end{multicols}
\end{exercice}

\begin{exercice}
Calculer les limites suivantes, si elles existent :
\comment{Formes indéterminées simples : croissances comparées et/ou terme dominant.}
\begin{multicols}{3}
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^2\sin x}{x^2+1}$,
\qq $\displaystyle \lim_{x\to +\infty} \ln\left(\frac{3x-2}{\sqrt[3]{x}}\right)$, %+composée
\qq $\displaystyle \lim_{x\to +\infty}e^{\frac{x^2-x}{x+2}}$,
\qq $\displaystyle \lim_{x\to+\infty} \dfrac{1+\ln x}{\ln x-1}$, %=1 %V. Collet ex.18 p.67
\qq $\displaystyle \lim_{x\to +\infty}\ \dfrac{\ln(xe^x)}{x}$,
\qq $\displaystyle \lim_{x\to +\infty} \frac{3x-2\ln x}{x+\ln x}$,
\qq $\displaystyle \lim_{x\to +\infty} \dfrac{\ln (1+e^x)}{x}$, % Fred
\qq $\displaystyle \lim_{x\to 0} xe^{\frac1x}$,
%\qq $\displaystyle \lim_{x\to +\infty} \sqrt{e^x+\ln x}+x^{2/3}$,
\qq $\displaystyle \lim_{x\to+\infty} \dfrac{x^3}{e^{\sqrt{x}}}$, %=0 %V. Collet ex.10,11 p.67
\qq $\displaystyle \lim_{x\to 0^+} x\ln\left(x^2 e^x\right)$,
%\qq $\displaystyle \lim_{x\to +\infty} \dfrac{x}{1+e^\frac1{(\sin(\arctan x)-1)}}$, %=+\infty % DS5 2005-2006
\qq $\displaystyle \lim_{x\to 0} \dfrac1{x^2}e^{\frac1x}$, %diverge
\qq $\displaystyle \lim_{x\to 1^+} (x-1)^{x-1}$,
\qq $\displaystyle \lim_{x\to +\infty} \dfrac{\ln(\ln x)}{x}$,
\qq $\displaystyle \lim_{x\to 0^+} (\ln x) e^{-\frac1x}$,
\qq $\displaystyle \lim_{x\to 0^+} x^x$, %=1 %V. Collet ex.10,11 p.67 %DM10 2004-2005
\qq $\displaystyle \lim_{x\to 0^+} x^{\frac1{\ln 3x}}$, % aubert et Papelier t.1 ex. 322 p.165
\qq $\displaystyle \lim_{x\to 0^+} \sqrt{x}{\ln^2 x}$, %=0 %V. Collet ex.10,11 p.67
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{ 3x}{5x-1}\dfrac{2x^2+1}{x^2+2x-1}$,
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{\sqrt{x+\sqrt{x}+1}}{x^{\frac14}\sqrt{\sqrt{x}+\frac1x+3}}$,
\qq $\displaystyle \lim_{x\to +\infty}\dfrac{\ln(1+x+x^2)}{1+x+x^2}$,
%\qq $\displaystyle \lim_{x\to 1} (x^2-3x+2)\ln(x-1)$.
%\qq $\displaystyle \lim_{x\to +\infty} \dfrac{\ln x + 3x^2 \ln(\ln x) +2x\ln x}{x^{\frac52}}$,
%\qq $\displaystyle \lim_{x\to +\infty} \dfrac{\sqrt{\ln x+1}}{\sqrt{\ln x}}+\dfrac{\ln^3 x-\ln x}{(\ln x+1)^3}$.
\end{multicols}
\end{exercice}

\begin{exercice}Donner l'ensemble de définition  des fonctions suivantes et dire si elles admettent des asymptotes verticales.
\comment{Asymptotes verticales}
\begin{multicols}{3}
\qq $f(x)=\frac{x+4}{x^2-9}$,
\qq $f(x)=\frac{2x^3-5}{x^2+x-6}$,
\qq $f(x) = \sqrt{2+x}-\sqrt{3-x}$,
\qq $f(x)=\frac{x-1}{x^2-1}$,
\qq $f(x) =\frac{x}{\vert x\vert}$,
\qq $f(x)= \tan (x)-\cos(x)$,
\qq $f(x) = \frac{\ln(x^2-1)}{\ln (\vert x-1\vert)}$.
\end{multicols}
\end{exercice}

\begin{exercice}Dire si les fonctions suivantes admettent des asymptotes obliques/horizontales et les déterminer.
\comment{Asymptotes diverses}
\begin{multicols}{3}
\qq $\displaystyle f(x)=x^2-2x$, %Condamine p.139
\qq $\displaystyle f(x)= \sqrt{x^2-1}$, %Condamine p.139
%\qq $\displaystyle \lim_{x\to -\infty} \sqrt{2x^2+3}$,
\qq $\displaystyle f(x)=\frac{\sqrt{x^2-3}}{x}$,
\qq $\displaystyle f(x)=1+x+\ln x$,
\qq $\displaystyle f(x)=xe^{\frac1x}$,
%limites en $x_0^+$ et $x_0^-$
\qq $\displaystyle f(x)=\frac{\cos(e^x)}{x^4-1}$, %analyse BCDE 2.11 p. 27
\qq $\displaystyle f(x)=\dfrac{\ln(|x|)}{\sqrt{x^2-1}}$,
\qq $\displaystyle f(x)=\dfrac{\pi^x}{x^2}$, %Condamine p.492
\qq $\displaystyle f(x)=\sin \dfrac1x$,
\qq $\displaystyle f(x)=\dfrac{x}{1+e^{\frac{1}{x}}}$, %Condamine p.492
%formes non indéterminées (0^+)^{+\infty}->0 et (0^+)^{-\infty}->+\infty$
%à Rajouter
\qq $\displaystyle f(x)=\dfrac{2x^2+1}{x^2+2}$,
\qq $\displaystyle f(x)=\dfrac{4x^5-4x^3+3}{x^4+3x^2-x+1}$,
\qq $\displaystyle f(x)=\dfrac{x^4+x}{x^3-1}$,
%\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^2-x}{x+2}$,
%\qq $\displaystyle \lim_{x\to +\infty}\dfrac{x^3-2}{x^3+1}$.
\end{multicols}
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Continuité, prolongement}

\begin{exercice}
\qq Montrer que la fonction suivante est continue sur $\R$.\\[1mm]
$f(x)=-2x+1$ si $x<0$,\quad
$f(x)=e^x$ si $0\leq x<1$,\quad
$f(x)=e x^2$ si $1\leq x$.
\qq Montrer que la fonction suivante est continue sur $[0,1]$.\\[1mm]
$f(x)=0$ si $x=0$,\quad
$f(x)=x+\frac{x\,\ln(x)}{1-x}$ si $0<x<1$,\quad
$f(x)=0$ si $x=1$.
\qq étudier la continuité de la fonction suivante.\\[1mm]
$f(x)=e^{\frac{1}{x}}$ si $x<0$,\quad
$f(x)=0$ si $x=0$,\quad
$f(x)=x\,\ln(x)-x$ si $x>0$.
\end{exercice}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dérivabilité}

\subsection{Calculs}

\begin{exercice}
 Pour chacune des fonctions suivantes, déterminer l'ensemble des points pour lesquels la fonction est dérivable, et calculer sa dérivée.
\begin{multicols}{4}
\qa $3e^x+\frac{4}{\sqrt[3]{x}}$,
\qa $\frac{e^x}{1+x}$,
\qa $\frac{\alpha x+\beta}{\gamma x+\delta}$,
\qa $\frac{x-\sqrt{x}}{1+\sqrt[3]{x}}$,
\qa $\frac{(2x-5)^3}{(8x^2-5)^3}$,
\qa $x^x$,
\qa $e^{\alpha \tan(x)}$,
\qa $\sin(\sin(\sin x))$,
\qa $\cos(\sin(\tan(\pi x)))$,
\qa $\sqrt{\frac{x}{x^2+4}}$,
\qa $\sqrt{x+\sqrt{x+\sqrt{x}}}$,
\qa $x^{x^x}$.
\end{multicols}
\end{exercice}


\begin{exercice}
Calculer les dérivées des fonctions suivantes (en précisant sur quel domaine ce calcul est valable), donner une équation de la tangente en $x_o$, puis lorsque c'est possible déterminer la position de la tangente par rapport à  la courbe à l'aide de la dérivée seconde. 
\begin{multicols}{2}
\qa $x\mapsto x^4-3x^3+x^2-4x+1$, $x_o=0$.
\qa $x\mapsto (x-3)^{10}$, $x_o=1$.
\qa $x\mapsto \cos(x)-\sin(x^2)$, $x_o=0$.
\qa $x\mapsto {1\over x}$, $x_o=1$.
\qa $x\mapsto {1\over 3x-5}$, $x_o=-1$.
\qa $x\mapsto x^3{1\over x^5}$, $x_o=1$.
\qa $x\mapsto \sqrt{x^2\sqrt[3]{x^{-10}}}$, $x_o=1$.
\qa $x\mapsto e^{1\over x}$, $x_o=1$.
\qa $x\mapsto \ln(x^2+1)$, $x_o=0$.
\qa $x\mapsto \sqrt{x+e^{x+1}}$, $x_o=2$.
\qa $x\mapsto x\ln(x)$, $x_o=1$.
\qa $x\mapsto \cos(e^{\sqrt{x}})$, $x_o=1$.
\qa $x\mapsto |\cos(x)|$, $x_o=\pi$.
\qa $x\mapsto \sqrt{|x^2-1|}$, $x_o=3$.
\qa $x\mapsto \tan(x^2-1)$, $x_o=0$.
\qa $x\mapsto \arctan (e^x)$, $x_o=0$.
\end{multicols}
\end{exercice}


\begin{exercice}
Considérons la fonction $f$ définie par\\[1mm]
$f(0) = 0 \text{ et } f(x) = x^2 \sin \frac{1}{x} \text{ si } x \ne 0.$
\qq Montrer que $f$ est dérivable sur $\mathbb{R}$.
\qq Calculer la dérivée de $f$ puis montrer que $f'$ n'est pas continue en $0$.
\end{exercice}


\begin{exercice}
Soit $f\colon\mathbb{R} \to \mathbb{R}$ la fonction définie par
$f(x) = \arctan \big(\frac{1+x}{1-x}\big) \text{ si } x \ne 1 \text{ et  } f(1) = - \frac{\pi}{2}$.
\qa étudier la continuité de $f$.
\qa Montrer que $f$ est dérivable en tout $x \ne 1$ mais n'est pas dérivable en $1$.
\qa Montrer que $f'$ a une limite finie quand $x$ tend vers $1$.
\qa Montrer que~:
$\forall x > 1, \quad   \arctan\left(\frac{1+x}{1-x}\right) = -\frac{3\pi}{4} + \arctan x.$
\end{exercice}

\begin{exercice}
Déterminer les extrema locaux et globaux de $f$ sur l'intervalle $I$ dans les cas suivants :
\begin{multicols}{2}
\qa $f(x)=x^3-6x^2+9x+1  \mbox{ et } I= [2,4]$,
\qa $f(x)=x\sqrt{1-x}  \mbox{ et } I= [-1,1]$,
\qa $f(x)=\frac{3x-4}{x^2+1}  \mbox{ et } I= [-2,2]$,
\qa $f(x)=(x^2+2x)^3  \mbox{ et } I= [-2,1]$,
\qa $f(x)=x+\sin(2x)  \mbox{ et } I= [0,\pi]$,
\qa $f(x)=\frac{\ln(x)}{x^2} \mbox{ et } I= [1,3]$.
\end{multicols}
\end{exercice}

\subsection{Application aux calculs de limites}
\comment{Faire apparaître des taux d'accroissement
(la règle de l'Hôpital n'est pas au programme).}

\begin{exercice}
Calculer les limites suivantes.
\begin{multicols}{3}
\qa $\lim_1 \frac{x^\beta-1}{x^\alpha-1}$,
\qa $\lim_0 \frac{\sin(4x)}{\tan(5x)}$,
\qa $\lim_0 \frac{e^{3x}-1}{x}$,
\qa $\lim_0 \frac{e^{x}-1}{x^3}$,
\qa $\lim_{+\infty} \frac{(\ln x )^2}{x}$,
\qa $\lim_0 \frac{5^{x}-3^x}{x}$,
\qa $\lim_{1} \frac{\ln x }{\sin(\pi x)}$,
\qa $\lim_1 \frac{\cos(x) \ln(x-1)}{\ln(e^x-e)}$,
\qa $\lim_{0} \frac{\tan(x) -x }{x^3}$,
\qa $\lim_{+\infty}\sqrt{x^2+x}-x$,
\qa $\lim_1 \left(\frac{x}{x-1}- \frac{1}{\ln x}\right)$,
\qa $\lim_{+\infty} \left(1+\frac{\alpha}{x}\right)^{\beta x}$.
\end{multicols}
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Étude de fonction, courbe représentative}
\comment{
  Les rappels de cours sur le plan d'étude de fonctions
  peuvent être faits avec une fonction bien choisie.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Parité, périodicité}

On dit qu'une fonction $f:\R\rightarrow\R$ d'ensemble de définition $\mathcal{D}$, est périodique de période $T\in\R$ si les deux conditions suivantes sont vérifiées :

(1) pour tout élément $x\in\mathbb{R}$, on a  $x\in\mathcal{D}$ si et seulement si $x+T\in\mathcal{D}$.

(2) pour tout $x\in\mathcal{D}$, on a $f(x+T)=f(x)$.
\bigskip

\begin{exercice} Soit $f:[0,1[\rightarrow \R$, $x\mapsto x^2$ et $\tilde{f}:\R\rightarrow\R$ le prolongement de $f$ en une fonction périodique de période $1$. Tracer le graphe de $\tilde{f}$.
\end{exercice}

\begin{exercice}
\qq Donner un exemple d'une fonction paire et périodique de période $1$.
\qq Donner un exemple d'une fonction impaire et périodique de période $-0,5$.
\end{exercice}

\begin{exercice} Soient $a$ et $b$ deux nombres réels. Montrer que si $f:\R\rightarrow\R$ et à la fois périodique de périodes $a$ et $b$ alors $f$ est périodique de période $a-b$.
\end{exercice}

\begin{exercice} Soit $k\in\R$. Si $f$ est définie sur $\R$ et périodique de période $T$, que peut-on dire de la fonction $x\mapsto f(kx)$ ?
\end{exercice}

\subsection{Etudes}
\comment{On a enlevé les références aux dérivées secondes.
On se contente d'étudier les positions relatives
aux tangentes/asymptotes par une étude de signe}.

\begin{exercice} On veut étudier la fonction $f:\R\rightarrow\R$ définie par~:
$\;f(x)=x^3+2x^2-x-2$.
\smallskip

On note $\mathcal{C}_f$ la courbe représentative de $f$.
\qq Donner les coordonnées des points d'intersection de $\mathcal{C}_f$ avec les axes $(Ox)$ et $(Oy)$.
\qq Donner les limites en l'infini.
\qq Calculer la dérivée de $f$. En déduire le tableau de variation de $f$.
\qq Tracer $\mathcal{C}_f$.
\end{exercice}

\begin{exercice}
Soit $f$ la fonction de variable réelle définie par~:
$\;f(x)=\ln\left(\frac{e^x-1}{x}\right)$.
\qa Quel est l'ensemble de définition de $f$ ?
\qa Montrer que $f$ se prolonge par continuité au point $0$ en une fonction $\varphi$ que l'on précisera.
\qa Montrer que $\varphi$ est dérivable en $0$ et donner l'équation de la tangente en $0$ au graphe de $\varphi$.
\qa étudier, au voisinage de $0$, la position du graphe de $\varphi$ par rapport à sa tangente.
\end{exercice}

\begin{exercice}
    On veut étudier la fonction $f:\R\rightarrow\R$ définie par~:
$\;f(x)=\frac{2x^2+x-3}{x-2}$.
\smallskip

On note $\mathcal{C}_f$ la courbe représentative de $f$.
\qq Déterminer l'ensemble de définition de $f$.
\qq Donner les coordonnées des points d'intersection de $\mathcal{C}_f$ avec les axes $(Ox)$ et $(Oy)$.
\qq Donner les limites en l'infini.
\qq Déterminer les asymptotes (verticales, horizontales/obliques).
\qq Calculer la dérivée de $f$. En déduire le tableau de variation de $f$.
\qq Donner l'allure de $\mathcal{C}_f$.
\end{exercice}

\begin{exercice} On veut étudier la fonction $f:\R\rightarrow\R$ définie par~:
$\;f(x)=x+\sqrt{|x^2-1|}$.
\smallskip

On note $\mathcal{C}_f$ la courbe représentative de $f$.
\qq Donner les coordonnées des points d'intersection de $\mathcal{C}_f$ avec les axes $(Ox)$ et $(Oy)$.
\qq La fonction $f$ est-elle continue sur $\R$, justifier votre réponse ?
\qq étudier les limites de $f$ en $+\infty$ et $-\infty$ et donner une équation des asymptotes si elles existent.
\qq étudier la dérivabilité de $f$. La courbe $\mathcal{C}_f$ admet-elle des tangentes aux points d'abscisse $-1$ et $1$ ? Justifier votre réponse.
\qq Calculer la dérivée de $f$ sur $]-\infty,-1[$, $]1,+\infty[$, puis sur $]-1,1[$. En déduire le tableau de variation de $f$.
%\qq étudier la dérivabilité de $f'$ et calculer $f''$. En déduire si $f$ est convexe, concave et dans quels intervalles.
\qq Tracer $\mathcal{C}_f$.
\end{exercice}

\begin{exercice} étudier la fonction $x\mapsto {\ln(x)\over x}$.
Utiliser son tableau de variation pour trouver, après avoir fixé $x>0$, le nombre de solutions de l'équation $x^y=y^x$ d'inconnue $y>0$.
\end{exercice}

\begin{exercice}  On pose : $A=\sqrt[3]{5\sqrt{2}+7}-\sqrt[3]{ 5\sqrt{2}-7}$.
\qq Montrer que : $\forall a,b\in \R, (a-b)^3+3(a-b)=a^3-b^3+(3-3ab)(a-b)$.
\qq étudier : $f:x\mapsto x^3+3x$, et donner son tableau de variation.
\qq Si  $a=\sqrt[3]{5\sqrt{2}+7}\ ,\ \ b=\sqrt[3]{5\sqrt{2}-7}$, calculer $ab$ et $a^3-b^3$.
\qq Prouver que $A=2$.
\end{exercice}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fonctions circulaires réciproques}
\comment{Hors programme.}

\cours{
    On note ${\rm arcsin}:[-1,1]\rightarrow [-\pi/2,\pi/2]$ la fonction inverse de $\sin :[-\pi/2,\pi/2]\rightarrow[-1,1]$. Donner l'allure de la courbe représentative de la fonction ${\rm arcsin}$.

    On note ${\rm arccos}:[-1,1]\rightarrow [0,\pi]$ la fonction inverse de $\cos :[0,\pi]\rightarrow[-1,1]$. Donner l'allure de la courbe représentative de la fonction ${\rm arccos}$.
}

\begin{exercice}Montrer les relations suivantes pour $x\in[-1,1]$.
\qq $\sin({\rm arccos}(x))=\sqrt{1-x^2}=\cos({\rm arcsin}(x))$.
\qq ${\rm arccos}(x)+{\rm arcsin}(x)=\frac{\pi}{2}$.
\end{exercice}

\begin{exercice} Résoudre l'équation suivante
\vskip-3mm
$$
{\rm arccos}(x)+{\rm arcsin}(x^2-x+1)=\frac{\pi}{2}.
$$
\end{exercice}

\cours{
On note ${\rm arctan}:\R\rightarrow ]-\pi/2,\pi/2[$ la fonction inverse de $\tan :]-\pi/2,\pi/2[\rightarrow\R$. Montrer que la fonction ${\rm arctan}$ est impaire puis donner l'allure de sa courbe représentative.
}

\begin{exercice}Montrer que pour tout $x\in]0,+\infty[$ on a
\vskip-3mm
$$
{\rm arctan}(1/x)+{\rm arctan}(x)=\pi/2.
$$
En déduire que si $x\in]-\infty,0[$, alors
\vskip-3mm
$$
{\rm arctan}(1/x)+{\rm arctan}(x)=-\pi/2.
$$
\end{exercice}

\begin{exercice}Montrer que pour tout $x\in]-1,1[$ on a
\vskip-3mm
$$
{\rm arctan}\left(\frac{x}{\sqrt{1-x^2}}\right)={\rm arcsin}(x).
$$

\end{exercice}

\begin{exercice}
    À partir des formules connues pour $\cos(a+b)$ et $\sin(a+b)$ montrer la formule suivante
$$
\tan(a+b)=\frac{\tan(a)+\tan(b)}{1-\tan(a)\tan(b)}.
$$
En utilisant cette formule montrer les identités suivantes :
\qq $2\,{\rm arctan}(1/2)={\rm arctan}(4/3)$.
\qq $\pi/4={\rm arctan}(1/2)+{\rm arctan}(1/5)+{\rm arctan}(1/8)$.
\end{exercice}


\end{document}

\newpage

\section{à mettre si besoin en RM1}

\begin{exercice}Dire parmi les fonctions suivantes celles qui sont bijectives.
%\begin{enumerate}
%\qq $[-1,1]\rightarrow [-1,0]$, $x\mapsto x^2-1$.
%\qq $]0,+\infty[\rightarrow \R$, $x\mapsto \ln(x)$.
%\qq $]0,1]\rightarrow ]0,+\infty[$, $x\mapsto |\ln(x)|$.
%\qq $]0,2[\rightarrow \R$,
%$\quad
%x\mapsto \begin{cases}\ln(x)&\text{ si } x\in]0,1]\\
%\frac{1}{\ln(2-x)} & \text{ si } x\in [1,2[.
%\end{cases}
%$
%\end{enumerate}
\\[1mm]
\hskip6.5pt
\begin{tabular}{p{0.47\linewidth}p{0.47\linewidth}}
1. $[-1,1]\rightarrow [-1,0]$, $x\mapsto x^2-1$.
&
2. $]0,+\infty[\rightarrow \R$, $x\mapsto \ln(x)$.
\\[1mm]
3. $]0,1]\rightarrow ]0,+\infty[$, $x\mapsto |\ln(x)|$.
&
4. $]0,2[\rightarrow \R$,
$\quad
x\mapsto \begin{cases}\ln(x)&\text{ si } x\in]0,1]\\
\frac{1}{\ln(2-x)} & \text{ si } x\in [1,2[.
\end{cases}
$
\end{tabular}
\end{exercice}

\begin{exercice}Donner un intervalle $A$ de $\R$ pour que les fonctions suivantes soient bijectives puis tracer leur courbe représentative.
%\begin{enumerate}
%\qq $A\rightarrow [-1,1]$, $x\mapsto \cos(x)$.
%\qq $A\rightarrow [-1,1]$, $x\mapsto \sin(x)$.
%\qq $A\rightarrow \R$, $x\mapsto\tan(x)$.
%\qq $A\rightarrow [0,2]$, $x\mapsto x^2$.
%\qq $A\rightarrow ]0,+\infty[$, $x\mapsto 1/x$.
%\qq $A\rightarrow ]0,1[$, $x\mapsto 2\ln(x)$.
%\qq $A\rightarrow ]0,2[$, $x\mapsto e^{4x}$.
%\qq $A\rightarrow [0,1]$, $x\mapsto |x^2-1|$.
%\end{enumerate}
\\[1mm]
\hskip6.5pt
\begin{tabular}{p{0.47\linewidth}p{0.47\linewidth}}
1. $A\rightarrow [-1,1]$, $x\mapsto \cos(x)$.
&
2. $A\rightarrow [-1,1]$, $x\mapsto \sin(x)$.
\\[1mm]
3. $A\rightarrow \R$, $x\mapsto\tan(x)$.
&
4. $A\rightarrow [0,2]$, $x\mapsto x^2$.
\\[1mm]
5. $A\rightarrow ]0,+\infty[$, $x\mapsto 1/x$.
&
6. $A\rightarrow ]0,1[$, $x\mapsto 2\ln(x)$.
\\[1mm]
7.  $A\rightarrow ]0,2[$, $x\mapsto e^{4x}$.
&
8. $A\rightarrow [0,1]$, $x\mapsto |x^2-1|$.
\end{tabular}
\end{exercice}
\medskip

\begin{exercice} Donner un intervalle $A$ de $\R$ pour que les fonctions suivantes  soient injectives.
%\begin{enumerate}
%\qq $f:A\rightarrow \R$, $x\mapsto \cos(2x)$.
%\qq $f:A\rightarrow \R$, $x\mapsto x^2+x+1$.
%\qq $f:A\rightarrow \R$, $x\mapsto x^3-3x^2+x+1$.
%\qq $f:A\rightarrow \R$, $x\mapsto  \ln(|\cos(x)|)$.
%\end{enumerate}
\\[1mm]
\hskip6.5pt
\begin{tabular}{p{0.47\linewidth}p{0.47\linewidth}}
1. $f:A\rightarrow \R$, $x\mapsto \cos(2x)$.
&
2. $f:A\rightarrow \R$, $x\mapsto x^2+x+1$.
\\[1mm]
3. $f:A\rightarrow \R$, $x\mapsto x^3-3x^2+x+1$.
&
4. $f:A\rightarrow \R$, $x\mapsto  \ln(|\cos(x)|)$.
\end{tabular}
\end{exercice}

\begin{exercice}Donner un intervalle $B$ de $\R$ pour que les fonctions suivantes  soient surjectives.
%\begin{enumerate}
%\qq $f:\R\rightarrow B$, $x\mapsto \cos(2x)$.
%\qq $f:\R\rightarrow B$, $x\mapsto x^2+x+1$.
%\qq $f:\R\rightarrow B$, $x\mapsto x^3-3x^2+x+1$.
%\qq $f:]-\pi/2,\pi/2[\rightarrow B$, $x\mapsto  \ln(|\cos(x)|)$.
%\end{enumerate}
\\[1mm]
\hskip6.5pt
\begin{tabular}{p{0.47\linewidth}p{0.47\linewidth}}
1. $f:\R\rightarrow B$, $x\mapsto \cos(2x)$.
&
2. $f:\R\rightarrow B$, $x\mapsto x^2+x+1$.
\\[1mm]
3. $f:\R\rightarrow B$, $x\mapsto x^3-3x^2+x+1$.
&
4. $f:]-\pi/2,\pi/2[\rightarrow B$, $x\mapsto  \ln(|\cos(x)|)$.
\end{tabular}
\end{exercice}


\begin{exercice}
\begin{enumerate}
\qq Montrer que si $g\circ f$ est injective alors $f$ doit être injective.
\qq Montrer que si $g\circ f$ est surjective, alors $g$ doit être aussi surjective.
\qq Montrer que la récriproque des deux assertions ci-dessus est fausse.
\end{enumerate}
\end{exercice}



\end{document}
