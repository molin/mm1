TD:=$(wildcard td*.tex)
SUJETS:=$(wildcard partiel*.tex) $(wildcard exam*.tex) $(wildcard rattrapage*.tex)
ENONCES:=$(TD) $(SUJETS)
PDF:=$(ENONCES:.tex=.pdf)
SOLS:=$(ENONCES:.tex=-sol.pdf)
PACKAGES:=td.cls

all: $(PDF) $(SOLS)

%.pdf: %.tex $(PACKAGES)
	pdflatex $<

%-sol.tex: %.tex $(PACKAGES)
	  sed '1 s/noanswer/answer/' $< > $@

clean:
	rm -f *.aux *.log *.out

clean-all: clean
	rm -f $(PDF) $(SOLS)
