# Supports enseignement MM1 + RM1

## Contenu

deux versions de feuilles de TD

- étudiant
- enseignant, avec indications

## Howto

### Exemple

Voir la feuille ``template-td.tex`` pour les environnements.

### Documentation

Une feuille commence avec
```
\documentclass[noanswer]{td}
```

On sectionne la feuille avec
```
\section{}
\subsection{}
```

L'environnement d'exercice est
```
\begin{exercice}
\qq énoncé question 1
\qq énoncé question 2
\end{exercice}
```

Pour numéroter les questions en lettres a,b,c, utiliser
```
\begin{exercice}
\qa énoncé question a
\qa énoncé question b
\end{exercice}
```

Utiliser l'environnement ``multicols`` pour faire plusieurs colonnes
```
\begin{exercice}
\begin{multicols}{2}
\qa énoncé question a
\qa énoncé question b
\end{multicols}
\end{exercice}
```

### Compilation

la commande ``make`` permet de générer à la fois
une feuille de td ``td.pdf`` et la version enseignant
``td-enseignant.pdf``.
